package hesge.intrumentor.jdt.exec;

import hesge.intrumentor.jdt.Instrumentor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class RunIntrumentor {
	public static void main(String[] args) {
		instrumentJabRef();
	}

	private static void instrumentJabRef() {
		String sourcePath = "/Volumes/Dummy/Temp/Tracing/Programs/jabref-v_2.10/src/java";
		String destinationPath = "/Volumes/Dummy/Temp/Tracing/Programs/jabref-v_2.10_inst/src/java";

		Instrumentor.instrumentFolderToFolder(sourcePath, destinationPath);
	}

	@SuppressWarnings("unused")
	private static void instrumentJHotel() {
		String sourcePath = "/Users/vicho/Documents/EclipseJunoWS/JHotel/src";
		String destinationPath = "/Users/vicho/Documents/EclipseJunoWS/JHotelInstrumented/src";

		Instrumentor.instrumentFolderToFolder(sourcePath, destinationPath);
	}

	@SuppressWarnings("unused")
	private static void instrumentArgoUML() {
		// Packages that make the project fail if instrumented, besides, don't seem
		// to compiled/added while packaging before run target (should I ignore
		// their build.xml as well?):
		// "argouml-core-diagrams-class2", "argouml-core-diagrams-structure2",
		// "argouml-core-diagrams-uml2"

		String[] argoUmlPackages =
			{ "argouml-app", "argouml-core-model", "argouml-core-model-euml", "argouml-core-model-mdr",
				"argouml-core-notation", "argouml-core-diagrams-state2", "argouml-core-diagrams-sequence2",
				"argouml-core-diagrams-activity2", "argouml-core-diagrams-deployment2", "argouml-core-umlpropertypanels",
				"argouml-core-transformer" };

		for (String argoUmlPackage : argoUmlPackages) {
			System.out.println("Instrumenting " + argoUmlPackage);

			String sourcePath = "/Users/vicho/Desktop/ArgoUML/src-ni/" + argoUmlPackage + "/src";
			String destinationPath = "/Users/vicho/Desktop/ArgoUML/src/" + argoUmlPackage + "/src";

			Instrumentor.instrumentFolderToFolder(sourcePath, destinationPath);
		}

		// Copy build.xml files enriched with the class-path to tracer.jar
		String[] argoUmlPackageWithAntBuildFile =
			{ "argouml-app", "argouml-build", "argouml-core-diagrams-activity2", "argouml-core-diagrams-class2",
				"argouml-core-diagrams-deployment2", "argouml-core-diagrams-sequence2", "argouml-core-diagrams-state2",
				"argouml-core-diagrams-structure2", "argouml-core-diagrams-uml2", "argouml-core-infra", "argouml-core-model",
				"argouml-core-model-euml", "argouml-core-model-mdr", "argouml-core-notation", "argouml-core-transformer",
				"argouml-core-umlpropertypanels" };

		System.out.println("Copying enhanced build.xml file for: ");
		int i = 0;
		for (String argoUmlPackage : argoUmlPackageWithAntBuildFile) {
			System.out.print(argoUmlPackage + ", ");

			try {
				copyFileUsingFileChannels(new File("/Users/vicho/Desktop/ArgoUML/i-buildxml/" + argoUmlPackage + "/build.xml"),
					new File("/Users/vicho/Desktop/ArgoUML/src/" + argoUmlPackage + "/build.xml"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (i++ % 3 == 2)
				System.out.println();
		}
		System.out.println();
	}

	private static void copyFileUsingFileChannels(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}
}
