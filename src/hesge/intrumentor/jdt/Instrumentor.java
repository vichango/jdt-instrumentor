package hesge.intrumentor.jdt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Stack;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

public class Instrumentor {
	/**
	 * Builds a string with the contents of the file at the given file path, using
	 * the given char-set name
	 * 
	 * @param file
	 * @param csName
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String file, String csName) throws IOException {
		Charset cs = Charset.forName(csName);
		return readFile(file, cs);
	}

	/**
	 * Builds a string with the contents of the file at the given file path, using
	 * the given char-set
	 * 
	 * @param file
	 * @param cs
	 * @return
	 * @throws IOException
	 */
	private static String readFile(String file, Charset cs) throws IOException {
		FileInputStream stream = new FileInputStream(file);

		try {
			Reader reader = new BufferedReader(new InputStreamReader(stream, cs));
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = reader.read(buffer, 0, buffer.length)) > 0)
				builder.append(buffer, 0, read);
			return builder.toString();
		} finally {
			stream.close();
		}
	}

	public static void instrumentFileInFile(String sourceFilePath, String sourceFileRelativePath,
		String destinationFilePath) {
		Document originalDocument = null;

		try {
			originalDocument = new Document(readFile(sourceFilePath, "utf-8"));
		} catch (IOException e) {
			System.err.println("Problem reading source file at " + sourceFilePath);
			e.printStackTrace();
		}

		if (originalDocument == null)
			System.err.println("Destination file not created for " + sourceFilePath);
		else {
			ASTParser parser = ASTParser.newParser(AST.JLS4);
			parser.setSource(originalDocument.get().toCharArray());
			parser.setKind(ASTParser.K_COMPILATION_UNIT);

			// Create the compilation unit and enable modification recording
			CompilationUnit compilationUnit = (CompilationUnit) parser.createAST(null);
			compilationUnit.recordModifications();

			// Visit the compilation unit, performing instrumentation
			AST ast = compilationUnit.getAST();
			compilationUnit.accept(new InstrumentorVisitor(sourceFileRelativePath, ast));

			// Export instrumented version
			try {
				TextEdit edits = compilationUnit.rewrite(originalDocument, null);
				Document resultDocument = new Document(originalDocument.get());
				edits.apply(resultDocument);

				File destinationFile = new File(destinationFilePath);
				BufferedWriter destinationWriter = new BufferedWriter(new FileWriter(destinationFile));
				destinationWriter.write(resultDocument.get());
				destinationWriter.close();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (MalformedTreeException e) {
				e.printStackTrace();
			} catch (BadLocationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void instrumentFolderToFolder(String sourcePath, String destinationPath) {
		// Clean source folder path
		File sourceFolder = null;
		try {
			sourceFolder = new File(sourcePath).getCanonicalFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Check destination folder path
		String destinationCleanPath = null;
		try {
			File destinationFile = new File(destinationPath);

			if (!destinationFile.exists())
				destinationFile.mkdirs();

			if (destinationFile.exists())
				destinationCleanPath = destinationFile.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (sourceFolder != null || destinationPath != null) {
			Stack<File> foldersToCheck = new Stack<File>();
			foldersToCheck.push(sourceFolder);

			while (!foldersToCheck.isEmpty()) {
				File folder = foldersToCheck.pop();

				File[] files = folder.listFiles();
				for (File file : files)
					if (file.isDirectory())
						foldersToCheck.push(file);
					else if (file.isFile()) {
						String relative = sourceFolder.toURI().relativize(file.getParentFile().toURI()).getPath();

						File destinationFolder = new File(destinationCleanPath + File.separator + relative);
						if (!destinationFolder.exists())
							destinationFolder.mkdir();

						if (file.getName().endsWith(".java"))
							instrumentFileInFile(file.getAbsolutePath(),
								file.getAbsolutePath().substring(sourceFolder.getAbsolutePath().length() + 1),
								destinationFolder.getAbsolutePath() + File.separator + file.getName());
					}
			}
		}
	}
}
