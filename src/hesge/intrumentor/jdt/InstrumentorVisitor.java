package hesge.intrumentor.jdt;

import java.util.List;
import java.util.Stack;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class InstrumentorVisitor extends ASTVisitor {
	private final AST ast;
	private final String classCompilationUnit;
	private String classPackage;

	private int anonymeClassNumber;
	private final Stack<String> classNameStack;
	private final Stack<MethodDeclaration> methodStack;

	/**
	 * @param filePath
	 *          Just for information as the AST has already been built
	 * @param ast
	 */
	public InstrumentorVisitor(String filePath, AST ast) {
		this.ast = ast;
		classCompilationUnit = filePath;

		classPackage = "<default>";

		anonymeClassNumber = 0;
		classNameStack = new Stack<String>();

		methodStack = new Stack<MethodDeclaration>();
	}

	private ExpressionStatement buildCallExpressionStatement(boolean endCall, String methodSignature, int start,
		int length) {
		// Build the position tag
		StringBuilder position = new StringBuilder(classCompilationUnit);
		position.append("[").append(start).append(":").append(start + length).append("]");

		// Build the call to get the current thread
		MethodInvocation currentThreadCall = ast.newMethodInvocation();
		currentThreadCall.setExpression(ast.newSimpleName("Thread"));
		currentThreadCall.setName(ast.newSimpleName("currentThread"));

		MethodInvocation currentThreadIdCall = ast.newMethodInvocation();
		currentThreadIdCall.setExpression(currentThreadCall);
		currentThreadIdCall.setName(ast.newSimpleName("getId"));

		// Build the call to the tracer method
		MethodInvocation methodInvocationCall = ast.newMethodInvocation();
		methodInvocationCall.setExpression(ast.newName("hesge.instrumentor.Trace"));
		methodInvocationCall.setName(ast.newSimpleName(endCall ? "traceCallEnd" : "traceCall"));

		StringLiteral classPackageString = ast.newStringLiteral();
		classPackageString.setLiteralValue(classPackage);
		methodInvocationCall.arguments().add(classPackageString);

		StringLiteral classNameString = ast.newStringLiteral();
		classNameString.setLiteralValue(classNameStack.peek());
		methodInvocationCall.arguments().add(classNameString);

		StringLiteral methodSignatureString = ast.newStringLiteral();
		methodSignatureString.setLiteralValue(methodSignature);
		methodInvocationCall.arguments().add(methodSignatureString);

		InfixExpression i = ast.newInfixExpression();

		methodInvocationCall.arguments().add(currentThreadIdCall);

		StringLiteral positionString = ast.newStringLiteral();
		positionString.setLiteralValue(position.toString());
		methodInvocationCall.arguments().add(positionString);

		return ast.newExpressionStatement(methodInvocationCall);
	}

	private String buildMethodSignature(MethodDeclaration node) {
		SimpleName methodName = node.getName();

		// Build the method's signature
		StringBuilder methodSignature = new StringBuilder(methodName.toString());
		methodSignature.append("(");

		List<SingleVariableDeclaration> methodParameters = node.parameters();
		if (methodParameters.size() > 0) {
			for (SingleVariableDeclaration parameter : methodParameters) {
				methodSignature.append(parameter.getType().toString());
				methodSignature.append(",");
			}

			methodSignature.deleteCharAt(methodSignature.length() - 1);
		}

		methodSignature.append(") AS ").append(node.getReturnType2() == null ? "void" : node.getReturnType2().toString());

		return methodSignature.toString();
	}

	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		classNameStack.push(classNameStack.get(0) + "$" + anonymeClassNumber++);

		return true;
	}

	@Override
	public void endVisit(AnonymousClassDeclaration node) {
		classNameStack.pop();
	}

	@Override
	public boolean visit(CompilationUnit node) {
		if (node.getPackage() != null)
			classPackage = node.getPackage().getName().toString();

		return true;
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		if (!node.isInterface()) {
			classNameStack.push(node.getName().toString());
			return true;
		} else
			return false;
	}

	@Override
	public void endVisit(TypeDeclaration node) {
		if (!node.isInterface())
			classNameStack.pop();
	}

	@Override
	public boolean visit(ReturnStatement node) {
		if (node.getParent().getNodeType() == ASTNode.BLOCK) {
			Block parent = (Block) node.getParent();

			int i = 0;
			for (Object statement : parent.statements()) {
				if (node.equals(statement))
					break;
				i++;
			}

			// Log the end of the call
			ExpressionStatement expressionStatementEnd =
				buildCallExpressionStatement(true, buildMethodSignature(methodStack.peek()), methodStack.peek()
					.getStartPosition(), methodStack.peek().getLength());

			parent.statements().add(i, expressionStatementEnd);
		}

		return true;
	}

	private boolean blockHasWhileTrueStatement(Block body) {
		if (body != null && body.statements().size() > 0)
			for (Object statement : body.statements()) {
				int nodeType = ((ASTNode) statement).getNodeType();

				if (nodeType == ASTNode.WHILE_STATEMENT && statement.toString().indexOf("while (true)") == 0)
					return true;
			}

		return false;
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		Block body = node.getBody();
		// Build the method's signature
		String methodSignature = buildMethodSignature(node);

		if (blockHasWhileTrueStatement(body)) {
			System.out.println("Method " + methodSignature + " in " + classCompilationUnit + " has a while (true)");
			return false;
		} else if (body != null && body.statements().size() > 0) {
			// Instrument after calls to constructors or super-constructors
			int insertAt = 0;

			int i = 1;
			for (Object statement : body.statements()) {
				int nodeType = ((ASTNode) statement).getNodeType();

				if (nodeType == ASTNode.SUPER_CONSTRUCTOR_INVOCATION || nodeType == ASTNode.CONSTRUCTOR_INVOCATION) {
					insertAt = i;
					break;
				}

				i++;
			}

			// Add tracer call for the method's call
			ExpressionStatement expressionStatementCall =
				buildCallExpressionStatement(false, methodSignature, node.getStartPosition(), node.getLength());
			body.statements().add(insertAt, expressionStatementCall);

			// Log the end of the call if return type is void
			if (node.getReturnType2() == null || node.getReturnType2().toString().equals("void")) {
				ExpressionStatement expressionStatementEnd =
					buildCallExpressionStatement(true, methodSignature.toString(), node.getStartPosition(), node.getLength());

				// Check if last statement is a throw and add before
				int lastNodeType = ((ASTNode) body.statements().get(body.statements().size() - 1)).getNodeType();

				if (lastNodeType != ASTNode.RETURN_STATEMENT)
					if (lastNodeType == ASTNode.THROW_STATEMENT)
						body.statements().add(body.statements().size() - 1, expressionStatementEnd);
					else
						body.statements().add(expressionStatementEnd);
			}

			methodStack.push(node);

			return true;
		} else
			return false;
	}

	@Override
	public void endVisit(MethodDeclaration node) {
		Block body = node.getBody();

		if (!blockHasWhileTrueStatement(body))
			if (body != null && body.statements().size() > 0)
				methodStack.pop();
	}
}
