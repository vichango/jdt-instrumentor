package hesge.instrumentor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Trace {

	private static final String fileName = "execution-trace.txt";

	private static PrintWriter out;
	static {
		try {
			out = new PrintWriter(new FileOutputStream(fileName), false);
		} catch (FileNotFoundException e) {
			System.err.println("Error while creating trace file " + e.getMessage());
		}
	}

	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				out.close();
			}
		});
	}

	public synchronized static void traceCall(String packageName, String className, String methodSignature,
		long thread, String position) {

		StringBuffer traceLine = new StringBuffer(packageName);
		traceLine.append(" ").append(className);
		traceLine.append(" [").append(thread).append("]");
		traceLine.append(" ").append(methodSignature);
		traceLine.append(" [0]");
		traceLine.append(" ").append(position);

		out.println(traceLine.toString());
	}

	public synchronized static void traceCallEnd(String packageName, String className, String methodSignature,
		long thread, String position) {

		StringBuffer traceLine = new StringBuffer("END");
		traceLine.append(" ").append(packageName);
		traceLine.append(" ").append(className);
		traceLine.append(" [").append(thread).append("]");
		traceLine.append(" ").append(methodSignature);
		traceLine.append(" [0]");
		traceLine.append(" ").append(position);

		out.println(traceLine.toString());
	}
}